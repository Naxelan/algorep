methods = ["add", "sub", "mul", "floordiv", "div", "mod", "pow", "lshift", "rshift", "and", "xor", "or", "radd", "rsub", "rmul", "rfloordiv", "rdiv", "rmod", "rpow", "rlshift", "rrshift", "rand", "rxor", "ror","iadd", "isub", "imul", "idiv", "ifloordiv", "imod", "ipow", "ilshift", "irshift", "iand", "ixor", "ior", "neg", "pos", "abs", "invert", "complex", "int", "long", "float", "oct", "hex", "lt", "le", "eq", "ne", "ge", "gt", "repr", "str", "bytes", "hash", "get", "set", "delete", "set_name", "len", "length_hint", "getitem", "setitem", "delitem", "missing", "iter", "reversed", "contains", "index", "round", "trunc", "floor", "ceil"]

for m in methods:
    print("    def __{}__(self, *args, **kwargs):".format(m))
    print("        return self.__getattr__(\"__{}__\")(*args, **kwargs)".format(m))
    print()
