from mpi4py import MPI
from middlerep.Communicator import Communicator

import sys


class Instancier():
    def __init__(self):
        self.com = Communicator(source=0, dest=0, recvtag=0)
        self.classes = {}

    def create_instance(self, data):
        id = data["id"]
        self.classes[id] = data["class"](*data["args"], **data["kwargs"])
        attrs, methods = self.get_attributes(id)
        self.com.class_info(id, attrs, methods, type(self.classes[data["id"]]))
        for node in data["nodes"][1:]:
            self.com.copy(id, self.classes[id], node)

    def copy(self, data):
        self.classes[data["class_id"]] = data["copy"]

    def get_attributes(self, class_id):
        attrs = []
        methods = []
        to_inspect = self.classes[class_id]
        for attr in dir(to_inspect):
            if callable(getattr(to_inspect, attr)):
                methods.append(attr)
            else:
                attrs.append(attr)
        return attrs, methods

    def check_method(self, data):
        method = data["method"]
        to_check = self.classes[data["class_id"]]
        if method in dir(to_check) and callable(getattr(to_check, method)):
            self.com.send(True, sendtag=data["class_id"])
        else:
            self.com.send(False, sendtag=data["class_id"])

    def check_attribute(self, data):
        attribute = data["attribute"]
        to_check = self.classes[data["class_id"]]
        self.com.send((attribute in dir(to_check)), sendtag=data["class_id"])

    def call_method(self, data):
        method = data["method"]
        to_call = self.classes[data["class_id"]]
        args = data["args"]
        kwargs = data["kwargs"]
        self.com.send(to_call.__getattribute__(method)(*args, **kwargs), sendtag=data["class_id"])
        for node in data["nodes"][1:]:
            self.com.copy(data["class_id"], to_call, node)

    def get_attribute(self, data):
        self.com.send(self.classes[data["class_id"]].__getattribute__(data["attribute"]), sendtag=data["class_id"])

    def set_attribute(self, data):
        self.classes[data["class_id"]].__setattr__(data["attribute"], data["value"])
        for node in data["nodes"][1:]:
            self.com.copy(data["class_id"], self.classes[data["class_id"]], node)

    def delete(self, id):
        self.classes.__delitem__(id)

    def run(self):
        while True:
            msg = self.com.recv()
            if msg["type"] == "instantiate":
                self.create_instance(msg["data"])
            elif msg["type"] == "check_method":
                self.check_method(msg["data"])
            elif msg["type"] == "call":
                self.call_method(msg["data"])
            elif msg["type"] == "get_attribute":
                self.get_attribute(msg["data"])
            elif msg["type"] == "set_attribute":
                self.set_attribute(msg["data"])
            elif msg["type"] == "delete":
                self.delete(msg["data"]["class_id"])
            elif msg["type"] == "copy":
                self.copy(msg["data"])
            elif msg["type"] == "exit":
                sys.exit(0)


def start():
    ins = Instancier()
    ins.run()


if MPI.COMM_WORLD.Get_rank() > 0:
    start()
