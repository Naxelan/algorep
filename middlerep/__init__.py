import middlerep.Instance
import middlerep.Instancier

import atexit
from mpi4py import MPI


def killall():
    for i in range(1, MPI.COMM_WORLD.Get_size()):
        MPI.COMM_WORLD.send({"type": "exit"}, dest=i, tag=0)

if  MPI.COMM_WORLD.Get_rank() == 0:
    atexit.register(killall)
