from mpi4py import MPI


class Communicator():
    def __init__(self, source=0, dest=1, sendtag=0, recvtag=0, debug=False):
        self.source = source
        self.dest = dest
        self.sendtag = sendtag
        self.recvtag = recvtag
        self.comm = MPI.COMM_WORLD
        self.debug = debug

    def instantiate(self, to_instantiate, args, kwargs, id, nodes):
        self.send({"type": "instantiate",
                   "data": {"id": id,
                            "nodes": nodes,
                            "class": to_instantiate,
                            "args": args,
                            "kwargs": kwargs}})

    def check_method(self, name, id):
        data = {"type": "check_method",
                "data": {"class_id": id,
                         "method": name}}
        self.send(data)

    def check_attribute(self, name, id):
        data = {"type": "check_attribute",
                "data": {"class_id": id,
                         "attribute": name}}
        self.send(data)

    def call(self, name, id, args, kwargs, nodes):
        data = {"type": "call",
                "data": {"method": name,
                         "class_id": id,
                         "nodes": nodes,
                         "args": args,
                         "kwargs": kwargs}}
        self.send(data)

    def get_attribute(self, name, id):
        data = {"type": "get_attribute",
                "data": {"class_id": id,
                         "attribute": name}}
        self.send(data)

    def set_attribute(self, name, id, value, nodes):
        data = {"type": "set_attribute",
                "data": {"class_id": id,
                         "nodes": nodes,
                         "attribute": name,
                         "value": value}}
        self.send(data)

    def class_info(self, id, attrs, methods, class_type):
        class_infos = {"class_id": id,
                       "class_attrs": attrs,
                       "class_methods": methods,
                       "class_type": class_type}
        self.send(class_infos, sendtag=id)

    def copy(self, id, copy, dest):
        data = {"type": "copy",
                "data": {"class_id": id,
                         "copy": copy}}
        self.send(data, dest=dest)

    def delete(self, id):
        data = {"type": "delete",
                "data": {"class_id": id}}
        self.send(data)

    def send(self, data, sendtag=None, dest=None):
        dest = self.dest if dest is None else dest
        sendtag = self.sendtag if sendtag is None else sendtag
        if self.debug:
            print("Sending {} to {} with tag {}".format(data, self.dest,
                                                        sendtag))
        self.comm.send(data, dest=dest, tag=sendtag)

    def recv(self):
        if self.debug:
            print("Receiving from {} with tag {}".format(self.source,
                                                         self.sendtag))
        return self.comm.recv(source=self.source, tag=self.recvtag)

    def setsendtag(self, tag):
        self.sendtag = tag

    def setrecvtag(self, tag):
        self.recvtag = tag

    def setdest(self, dest):
        self.dest = dest

    def setsource(self, source):
        self.source = source
