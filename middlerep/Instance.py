from middlerep.Communicator import Communicator
from mpi4py import MPI

next_id = 1
class_counter = [0] * (MPI.COMM_WORLD.Get_size() - 1)
REDUNDANCY = 2


class Instance():
    def __init__(self, to_instantiate, *args, **kwargs):
        global next_id
        object.__setattr__(self, "_id", next_id)
        next_id += 1
        nodes = []
        for i in range(REDUNDANCY):
            nodes.append(self._choose_node())
        object.__setattr__(self, "com", Communicator(source=nodes[0], dest=nodes[0], recvtag=next_id - 1))
        self.com.instantiate(to_instantiate, args, kwargs, next_id - 1, nodes)
        data = self.com.recv()
        object.__setattr__(self, "_nodes", nodes)
        object.__setattr__(self, "_methods", data["class_methods"])
        object.__setattr__(self, "_attributes", data["class_attrs"])
        object.__setattr__(self, "_type", data["class_type"])

    def _switch_node(self):
        nodes = object.__getattribute__(self, "_nodes")
        nodes.pop(0)
        self.com.setdest(nodes[0])
        self.com.setsource(nodes[0])
        nodes.append(self._choose_node())

    def _choose_node(self):
        global class_counter
        index = class_counter.index(min(class_counter))
        class_counter[index] += 1
        return index + 1

    def _check_methods_presence(self, name):
        self.com.check_method(name, object.__getattribute__(self, "_id"))
        if self.com.recv():
            self._methods.add(name)
            return True
        return False

    def _check_attributes_presence(self, name):
        self.com.check_attribute(name, object.__getattribute__(self, "_id"))
        if self.com.recv():
            self._attributes.add(name)
            return True
        return False

    def __getattribute__(self, name):
        # print("getattribute", name)
        if name == "__class__":
            return self._type
        return object.__getattribute__(self, name)

    def __del__(self):
        global class_counter
        nodes = object.__getattribute__(self, "_nodes")
        for node in nodes:
            class_counter[node - 1] -= 1
        if "__del__" in object.__getattribute__(self, "_methods"):
            self.com.call("__del__", object.__getattribute__(self, "_id"), [], {},
                          object.__getattribute__(self, "_nodes"))
        for node in object.__getattribute__(self, "_nodes"):
            self.com.setdest(node)
            self.com.delete(object.__getattribute__(self, "_id"))

    def __getattr__(self, name):
        #        print("getattr", name)
        methods = object.__getattribute__(self, "_methods")
        if name in methods or self._check_methods_presence(name):
            def wrapper(*args, **kwargs):
                # print("Calling {} with {} and {}".format(name, args, kwargs))
                self.com.call(name, object.__getattribute__(self, "_id"), args, kwargs,
                              object.__getattribute__(self, "_nodes"))
                return self.com.recv()

            return wrapper
        attributes = object.__getattribute__(self, "_attributes")
        if name in attributes or self._check_attributes_presence(name):
            self.com.get_attribute(name, object.__getattribute__(self, "_id"))
            return self.com.recv()
        raise AttributeError

    def __setattr__(self, name, value):
        # print("setattr", name)
        methods = object.__getattribute__(self, "_methods")
        if name in methods or self._check_methods_presence(name):
            self.com.set_attribute(name, object.__getattribute__(self, "_id"), value,
                                   object.__getattribute__(self, "_nodes"))
            return

        attributes = object.__getattribute__(self, "_attributes")
        if name in attributes:
            self.com.set_attribute(name, object.__getattribute__(self, "_id"), value,
                                   object.__getattribute__(self, "_nodes"))
            return
        raise AttributeError

    """
        These functions have to be implemented in the Class and not in the
        instance of the Class
    """

    def __add__(self, *args, **kwargs):
        return self.__getattr__("__add__")(*args, **kwargs)

    def __sub__(self, *args, **kwargs):
        return self.__getattr__("__sub__")(*args, **kwargs)

    def __mul__(self, *args, **kwargs):
        return self.__getattr__("__mul__")(*args, **kwargs)

    def __floordiv__(self, *args, **kwargs):
        return self.__getattr__("__floordiv__")(*args, **kwargs)

    def __div__(self, *args, **kwargs):
        return self.__getattr__("__div__")(*args, **kwargs)

    def __mod__(self, *args, **kwargs):
        return self.__getattr__("__mod__")(*args, **kwargs)

    def __pow__(self, *args, **kwargs):
        return self.__getattr__("__pow__")(*args, **kwargs)

    def __lshift__(self, *args, **kwargs):
        return self.__getattr__("__lshift__")(*args, **kwargs)

    def __rshift__(self, *args, **kwargs):
        return self.__getattr__("__rshift__")(*args, **kwargs)

    def __and__(self, *args, **kwargs):
        return self.__getattr__("__and__")(*args, **kwargs)

    def __xor__(self, *args, **kwargs):
        return self.__getattr__("__xor__")(*args, **kwargs)

    def __or__(self, *args, **kwargs):
        return self.__getattr__("__or__")(*args, **kwargs)

    def __radd__(self, *args, **kwargs):
        return self.__getattr__("__radd__")(*args, **kwargs)

    def __rsub__(self, *args, **kwargs):
        return self.__getattr__("__rsub__")(*args, **kwargs)

    def __rmul__(self, *args, **kwargs):
        return self.__getattr__("__rmul__")(*args, **kwargs)

    def __rfloordiv__(self, *args, **kwargs):
        return self.__getattr__("__rfloordiv__")(*args, **kwargs)

    def __rdiv__(self, *args, **kwargs):
        return self.__getattr__("__rdiv__")(*args, **kwargs)

    def __rmod__(self, *args, **kwargs):
        return self.__getattr__("__rmod__")(*args, **kwargs)

    def __rpow__(self, *args, **kwargs):
        return self.__getattr__("__rpow__")(*args, **kwargs)

    def __rlshift__(self, *args, **kwargs):
        return self.__getattr__("__rlshift__")(*args, **kwargs)

    def __rrshift__(self, *args, **kwargs):
        return self.__getattr__("__rrshift__")(*args, **kwargs)

    def __rand__(self, *args, **kwargs):
        return self.__getattr__("__rand__")(*args, **kwargs)

    def __rxor__(self, *args, **kwargs):
        return self.__getattr__("__rxor__")(*args, **kwargs)

    def __ror__(self, *args, **kwargs):
        return self.__getattr__("__ror__")(*args, **kwargs)

    def __iadd__(self, *args, **kwargs):
        return self.__getattr__("__iadd__")(*args, **kwargs)

    def __isub__(self, *args, **kwargs):
        return self.__getattr__("__isub__")(*args, **kwargs)

    def __imul__(self, *args, **kwargs):
        return self.__getattr__("__imul__")(*args, **kwargs)

    def __idiv__(self, *args, **kwargs):
        return self.__getattr__("__idiv__")(*args, **kwargs)

    def __ifloordiv__(self, *args, **kwargs):
        return self.__getattr__("__ifloordiv__")(*args, **kwargs)

    def __imod__(self, *args, **kwargs):
        return self.__getattr__("__imod__")(*args, **kwargs)

    def __ipow__(self, *args, **kwargs):
        return self.__getattr__("__ipow__")(*args, **kwargs)

    def __ilshift__(self, *args, **kwargs):
        return self.__getattr__("__ilshift__")(*args, **kwargs)

    def __irshift__(self, *args, **kwargs):
        return self.__getattr__("__irshift__")(*args, **kwargs)

    def __iand__(self, *args, **kwargs):
        return self.__getattr__("__iand__")(*args, **kwargs)

    def __ixor__(self, *args, **kwargs):
        return self.__getattr__("__ixor__")(*args, **kwargs)

    def __ior__(self, *args, **kwargs):
        return self.__getattr__("__ior__")(*args, **kwargs)

    def __neg__(self, *args, **kwargs):
        return self.__getattr__("__neg__")(*args, **kwargs)

    def __pos__(self, *args, **kwargs):
        return self.__getattr__("__pos__")(*args, **kwargs)

    def __abs__(self, *args, **kwargs):
        return self.__getattr__("__abs__")(*args, **kwargs)

    def __invert__(self, *args, **kwargs):
        return self.__getattr__("__invert__")(*args, **kwargs)

    def __complex__(self, *args, **kwargs):
        return self.__getattr__("__complex__")(*args, **kwargs)

    def __int__(self, *args, **kwargs):
        return self.__getattr__("__int__")(*args, **kwargs)

    def __long__(self, *args, **kwargs):
        return self.__getattr__("__long__")(*args, **kwargs)

    def __float__(self, *args, **kwargs):
        return self.__getattr__("__float__")(*args, **kwargs)

    def __oct__(self, *args, **kwargs):
        return self.__getattr__("__oct__")(*args, **kwargs)

    def __hex__(self, *args, **kwargs):
        return self.__getattr__("__hex__")(*args, **kwargs)

    def __lt__(self, *args, **kwargs):
        return self.__getattr__("__lt__")(*args, **kwargs)

    def __le__(self, *args, **kwargs):
        return self.__getattr__("__le__")(*args, **kwargs)

    def __eq__(self, *args, **kwargs):
        return self.__getattr__("__eq__")(*args, **kwargs)

    def __ne__(self, *args, **kwargs):
        return self.__getattr__("__ne__")(*args, **kwargs)

    def __ge__(self, *args, **kwargs):
        return self.__getattr__("__ge__")(*args, **kwargs)

    def __gt__(self, *args, **kwargs):
        return self.__getattr__("__gt__")(*args, **kwargs)

    def __repr__(self, *args, **kwargs):
        return self.__getattr__("__repr__")(*args, **kwargs)

    def __str__(self, *args, **kwargs):
        return self.__getattr__("__str__")(*args, **kwargs)

    def __bytes__(self, *args, **kwargs):
        return self.__getattr__("__bytes__")(*args, **kwargs)

    def __hash__(self, *args, **kwargs):
        return self.__getattr__("__hash__")(*args, **kwargs)

    def __get__(self, *args, **kwargs):
        return self.__getattr__("__get__")(*args, **kwargs)

    def __set__(self, *args, **kwargs):
        return self.__getattr__("__set__")(*args, **kwargs)

    def __delete__(self, *args, **kwargs):
        return self.__getattr__("__delete__")(*args, **kwargs)

    def __set_name__(self, *args, **kwargs):
        return self.__getattr__("__set_name__")(*args, **kwargs)

    def __len__(self, *args, **kwargs):
        return self.__getattr__("__len__")(*args, **kwargs)

    def __length_hint__(self, *args, **kwargs):
        return self.__getattr__("__length_hint__")(*args, **kwargs)

    def __getitem__(self, *args, **kwargs):
        return self.__getattr__("__getitem__")(*args, **kwargs)

    def __setitem__(self, *args, **kwargs):
        return self.__getattr__("__setitem__")(*args, **kwargs)

    def __delitem__(self, *args, **kwargs):
        return self.__getattr__("__delitem__")(*args, **kwargs)

    def __missing__(self, *args, **kwargs):
        return self.__getattr__("__missing__")(*args, **kwargs)

    def __iter__(self, *args, **kwargs):
        return self.__getattr__("__iter__")(*args, **kwargs)

    def __reversed__(self, *args, **kwargs):
        return self.__getattr__("__reversed__")(*args, **kwargs)

    def __contains__(self, *args, **kwargs):
        return self.__getattr__("__contains__")(*args, **kwargs)

    def __index__(self, *args, **kwargs):
        return self.__getattr__("__index__")(*args, **kwargs)

    def __round__(self, *args, **kwargs):
        return self.__getattr__("__round__")(*args, **kwargs)

    def __trunc__(self, *args, **kwargs):
        return self.__getattr__("__trunc__")(*args, **kwargs)

    def __floor__(self, *args, **kwargs):
        return self.__getattr__("__floor__")(*args, **kwargs)

    def __ceil__(self, *args, **kwargs):
        return self.__getattr__("__ceil__")(*args, **kwargs)
