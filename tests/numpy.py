import middlerep
import numpy as np

#Numpy tests
insnumpy = middlerep.Instance.Instance(np.eye, 3)
print(insnumpy)
print(insnumpy * 2)
print(insnumpy + 3)
print(insnumpy.shape)
